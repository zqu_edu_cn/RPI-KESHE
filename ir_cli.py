#!/bin/python3
from ir import *
import wiringpi

ir_device = IR()

def control(op:int,mode:int ,degree:int):
    ir_data = Gree.create_ir_data(
        key=op, mode=mode, temp=degree)
    
    ir_device.send(1, ir_data[0])
    
    wiringpi.delayMicroseconds(40000)
    
    ir_device.send(1, ir_data[1])

"""
# 输入格式：
1. 开启制冷，并且设置温度
  COLD <int>
  如：
  COLD 21
  空调开启制冷，并且设置为21度
2. 开启制热
  HOT <int>
  如：
  HOT 28
  空调开启制热，并且设置为28度
3. 关闭空调
  OFF

"""
if __name__ == "__main__":
    try:
        while True:
            inp = input().split()
            if inp[0] == "COLD":
                degree = int(inp[1])
                control(op=Gree.KEY_ON, mode=Gree.MODE_REFRIGERATION, degree=degree)
            elif inp[0] == "HOT":
                degree = int(inp[1])
                control(op=Gree.KEY_ON, mode=Gree.MODE_HEATING, degree=degree)
            elif inp[0] == "OFF":
                control(op=Gree.KEY_OFF, mode=Gree.MODE_REFRIGERATION, degree=21)
            print(inp)
    except EOFError:
        pass

