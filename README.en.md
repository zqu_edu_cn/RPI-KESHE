# Raspberry Pi Smart Air Conditioning Control
## Project Goal
This project aims to develop a smart air conditioning control system based on Raspberry Pi, which allows users to automate the air conditioning using an electronic system. By using a temperature module, the system can sense indoor environment temperature and humidity and calculate the perceived temperature. Based on this information, the system will control the air conditioning operations through an infrared emission module.

## Project Methodology
The project primarily uses Raspberry Pi for design and development.

- In hardware terms, the project requires:

  1. Temperature sensor
  2. Infrared emission/reception module
- In software terms, the project's software is divided into the following modules:

  1. Data collection module: Connects the temperature sensor, collects environmental temperature and humidity, calculates perceived temperature and streams the results to the decision-making module.
  2. Decision-making module: Receives the data stream from the data collection module, decides whether to turn the air conditioning on or off, and sends the decision to the infrared emission module.
  3. Infrared emission module: Receives the decision-making module's decision and emits an infrared signal to control the air conditioning's power switch.
## Timeline
- Week 1: Collect necessary materials and tools, set up the development environment, and research relevant materials.
- Week 2: Develop the necessary software for the project.
- Week 3: System testing evaluation, create PPT and demonstration video.

## Risks and Feasibility

This project requires the use of hardware devices such as Raspberry Pi, temperature sensors, and infrared emission modules, and the development of related programs to control the hardware. The difficulty of this project lies in emitting the infrared signal and parsing the air conditioning control signal. For the infrared signal emission, Raspberry Pi's strong processing capabilities and rich ecological resources can be relied upon. As for the air conditioning control signal, it can be recorded using an air conditioning remote control.