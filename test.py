import math

last_error=0 #上次误差
V=1.0#风速
set_temp=26#默认温度


# 初始化PID参数和变量
proportional = 0.0
integral= 0.0
derivative = 0.0
kp=1.0
ki=0.1
kd=0.01

while True:
    try:
        # 接受用户输入
        ss=input()
        current_temp,current_humid=ss.split()
        current_temp=float(current_temp)
        current_humid=float(current_humid)

        # 计算体感温度
        e=current_humid/100.0*6.105*math.exp(17.27*current_temp/(237.7+current_temp))
        heat_index=1.07*current_temp+0.2*e-0.65*V-2.7
        #print("计算出的体感温度为：", heat_index)
        
        def pid_control(setpoint, kp, ki, kd):
            global integral
            global last_error
            global derivative
            global proportional
    

            # 计算误差并累加
            error = heat_index-setpoint
            proportional = kp * error 
            integral += ki * error
            derivative = kd * (error - last_error)

            # 将三个项相加得到PID输出值
            output = proportional + integral + derivative
        
            # 重置错误值
            last_error = error
            
            # 设置环境控制设备
            return output
        
        expect_temp=15
        if heat_index<expect_temp:
            cold=True
        elif heat_index>expect_temp+10:
            cold=False
        else:
            print("OFF")
            continue
        if cold==False:
            set_temp=26-0.2*(heat_index-26)-0.3*pid_control(set_temp,kp,ki,kd)
        else:
            set_temp=27+0.2*(16-heat_index)+0.03*pid_control(set_temp,kp,ki,kd)
        temp=round(set_temp)
        print(("COLD "+str(temp)) if cold==False else ("HOT "+str(round(temp))))
        
        '''
        # 根据体感温度控制空调的最佳温度
        if heat_index < 10:
            mode=True    
            cold=True
            set_temp=(9-heat_index)*0.34+24
        elif heat_index>26:
            mode=True
            cold=False
            set_temp=27-(heat_index-26)*0.68
        '''
        '''
        if mode==True:
            print(("COLD "+str(round(set_temp))) if cold==False else ("HOT "+str(round(set_temp))))
            #print("控制空调的最佳温度为：",set_temp)
        else:
            print("OFF")

        #print("计算出的体感温度为：", heat_index)
    '''
    except EOFError:
        break 